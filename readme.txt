=== Spice Portfolio ===

Contributors: spicethemes
Tags: Portfolio, Filter Portfolio
Requires at least: 5.3
Requires PHP: 5.2
Tested up to: 5.8
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

A complete extension to display your portfolio and creative work in a beautiful way.

== Changelog ==

@Version 1.0
* Updated freemius directory.

@Version 0.2.1
* Freemius code updated.

@Version 0.1
* Initial Release.

======= External Resources =======

Font Awesome:
Copyright: (c) Dave Gandy
License: https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License)
Source: https://fontawesome.com

Select Control:
Copyright: (c) Kevin Brown, Igor Vaynberg
License: MIT License
Source: https://github.com/select2/select2/tree/develop/dist/js

Jquery UI:
Copyright: (c) jQuery Foundation
License: MIT License
Source: http://jqueryui.com