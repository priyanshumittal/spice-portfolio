<?php
/*
* Plugin Name:			Spice Portfolio
* Plugin URI:  
* Description: 			A complete extension to display your portfolio and creative work in a beautiful way.
* Version:     			1.0
* Requires at least: 	5.3
* Requires PHP: 		5.2
* Tested up to: 		5.8
* Author:      			Spicethemes
* Author URI:  			https://spicethemes.com
* License: 				GPLv2 or later
* License URI: 			https://www.gnu.org/licenses/gpl-2.0.html
* Text Domain: 			spice-portfolio
* Domain Path:  		/languages
*/

if ( ! function_exists( 'sp_fs' ) ) {
    // Create a helper function for easy SDK access.
    function sp_fs() {
        global $sp_fs;

        if ( ! isset( $sp_fs ) ) {
            // Include Freemius SDK.
            if ( function_exists('olivewp_companion_activate') && defined( 'OWC_PLUGIN_DIR' ) && file_exists(OWC_PLUGIN_DIR . '/inc/freemius/start.php') ) {
                // Try to load SDK from olivewp companion folder.
                require_once OWC_PLUGIN_DIR . '/inc/freemius/start.php';
            } else if ( function_exists('olivewp_plus_activate') && defined( 'OLIVEWP_PLUGIN_DIR' ) && file_exists(OLIVEWP_PLUGIN_DIR . '/freemius/start.php') ) {
                // Try to load SDK from premium olivewp companion plugin folder.
                require_once OLIVEWP_PLUGIN_DIR . '/freemius/start.php';
            } else {
                require_once dirname(__FILE__) . '/freemius/start.php';
            }

            $sp_fs = fs_dynamic_init( array(
                'id'                  => '10572',
                'slug'                => 'spice-portfolio',
                'premium_slug'        => 'spice-portfolio',
                'type'                => 'plugin',
                'public_key'          => 'pk_0381788ca1e2cd246fd2f8921eb93',
                'is_premium'          => true,
                'is_premium_only'     => true,
                'has_paid_plans'      => true,
                'is_org_compliant'    => false,
                'menu'                => array(
                    'slug'           => 'edit.php?post_type=spice_portfolio',
                    'first-path'     => 'edit.php?post_type=spice_portfolio',
                    'support'        => false,
                ),
                // Set the SDK to work in a sandbox mode (for development & testing).
                // IMPORTANT: MAKE SURE TO REMOVE SECRET KEY BEFORE DEPLOYMENT.
                ) );
        }

        return $sp_fs;
    }
	sp_fs();
}

// Exit if accessed directly
if( ! defined('ABSPATH'))
{
	die('Do not open this file directly.');
}


/**
 * Main Spice_Portfolio Class
 *
 * @class Spice_Portfolio
 * @version 0.1
 * @since 0.1
 * @package Spice_Portfolio
 */

final class Spice_Portfolio {

	/**
	 * The version number.
	 *
	 * @var     string
	 * @access  public
	 * @since   0.1
	 */
	public $version;


	/**
	 * Constructor function.
	 *
	 * @access  public
	 * @since   0.1
	 * @return  void
	 */
	public function __construct() {
		$this->plugin_url  = plugin_dir_url( __FILE__ );
		$this->plugin_path = plugin_dir_path( __FILE__ );
		$this->version     = '0.1';

		define( 'SPICE_PORTFOLIO_URL', $this->plugin_url );
		define( 'SPICE_PORTFOLIO_PATH', $this->plugin_path );
		define( 'SPICE_PORTFOLIO_VERSION', $this->version );

		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );

		//Register spice_portfolio CPT
		require_once SPICE_PORTFOLIO_PATH . '/include/admin/spice-portfolio-cpt.php';

		//Add Meta boxes for spice_portfolio CPT
		require_once SPICE_PORTFOLIO_PATH . '/include/admin/spice-portfolio-meta.php';

		//Register taxomony
		require_once SPICE_PORTFOLIO_PATH . '/include/admin/spice-portfolio-taxonomies.php';

		//Register portfolio_shortcodes CPT
		require_once SPICE_PORTFOLIO_PATH . '/include/admin/spice-portfolio-shortcode-cpt.php';

		//Output file to view portfolio
		require_once SPICE_PORTFOLIO_PATH . '/include/view/shortcode.php';

		//Font file
		require_once SPICE_PORTFOLIO_PATH . '/include/admin/spice-portfolio-fonts.php';
	}

	/**
	 * Load the localisation file.
	 *
	 * @access  public
	 * @since   0.1
	 * @return  void
	 */
	public function load_plugin_textdomain() {
		load_plugin_textdomain( 'spice-portfolio', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
	}


}

new Spice_Portfolio;