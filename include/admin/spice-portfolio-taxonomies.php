<?php
//register portfolio custom post type taxonomy
function spice_portfolio_taxonomy() {
	
	register_taxonomy('sp_portfolio_categories', 'spice_portfolio',
    array(  'hierarchical' => true,
			'show_in_nav_menus' => true,
			//'rewrite' => array('slug' => $project_category ),
            'label' => 'Categories',
            'query_var' => true));
	if((isset($_POST['action'])) && (isset($_POST['taxonomy']))){		
		wp_update_term($_POST['tax_ID'], 'spice_categories', array(
		  'name' => $_POST['name'],
		  'slug' => $_POST['slug']
		));
	} 
	
	//update category
	if(isset($_POST['action']) && isset($_POST['taxonomy']) )
	{	wp_update_term($_POST['tag_ID'], 'sp_portfolio_categories', array(
		  'name' => $_POST['name'],
		  'slug' => $_POST['slug'],
		  'description' =>$_POST['description']
		));
	}
	// Delete default category
	if(isset($_POST['action']) && isset($_POST['tag_ID']) )
	{	if(($_POST['tag_ID'] == $defualt_tex_id) &&$_POST['action']	 =="delete-tag")
		{	
			delete_option('custom_texo_spice'); 
		} 
	}	
	
}
add_action( 'init', 'spice_portfolio_taxonomy' );