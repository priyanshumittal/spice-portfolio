<?php
/**
* Register Post Type
*/

if ( ! class_exists( 'Spice_Portfolio_Shortcode_Post_Type' ) ) {

  class Spice_Portfolio_Shortcode_Post_Type
  {

    public function __construct()
    {
      add_action( 'init', array( $this, 'shortcodes_post_type' ) );
      {
        add_action( 'admin_enqueue_scripts', array( $this,'spice_portfolio_load_script') );
        add_action( 'wp_enqueue_scripts', array( $this,'spice_portfolio_front_script') );
        add_action( 'add_meta_boxes', array( $this, 'spice_portfolio_shortcodes_meta_fn') );
        add_action('save_post',array( $this, 'spice_portfolio_shortcode_meta_save') );
      }
    }

     /**
     * Load Admin Script
     *
     * @since 0.1
     */
     public function spice_portfolio_load_script()
      {
        wp_enqueue_style('wp-color-picker');
        wp_enqueue_script('spice-portfolio-color', SPICE_PORTFOLIO_URL.'assets/js/color-picker.js', array( 'wp-color-picker' ), false, true );
        wp_enqueue_style('spice-portfolio', SPICE_PORTFOLIO_URL.'assets/css/style.css');
        wp_enqueue_style('spice-portfolio-select', SPICE_PORTFOLIO_URL.'assets/css/select2.min.css');
        wp_enqueue_style('jquery-custom-style', SPICE_PORTFOLIO_URL.'assets/css/jquery-ui.css'); 
        wp_enqueue_script('jquery-ui-tabs');
        wp_enqueue_script('jquery-ui-slider');
        wp_enqueue_script('spice-portfolio-select', SPICE_PORTFOLIO_URL . 'assets/js/select.min.js', array('jquery'));
        wp_enqueue_script('spice-custom-tabs', SPICE_PORTFOLIO_URL . 'assets/js/custom.js', array('jquery'));
      }


     /**
     * Load Front Side Script
     *
     * @since 0.1
     */  
     public function spice_portfolio_front_script()
     {
      wp_enqueue_style('spice-font-awesom', SPICE_PORTFOLIO_URL.'assets/css/font-awesome/css/all.min.css');
      wp_enqueue_style('spice-portfolio-front-style', SPICE_PORTFOLIO_URL.'assets/css/front-style.css');
      wp_enqueue_script('spice-portfolio-isotope', SPICE_PORTFOLIO_URL .'assets/js/isotope.min.js', array('jquery'));
     }


    /**
     * Register shortcodes post type
     *
     * @since 0.1
     */
    public static function shortcodes_post_type() {
      register_post_type( 'portfolio_shortcodes', apply_filters( 'spice_portfolio_shortcodes_args', array(
        'labels' => array(
          'name'          => esc_html__( 'Shortcodes', 'spice-portfolio' ),
          'singular_name'     => esc_html__( 'Shortcode', 'spice-portfolio' ),
        ),
        'public'             => true,
        'hierarchical'       => false,
        'show_ui'            => true,
        'show_in_menu'       => 'edit.php?post_type=spice_portfolio',
        'show_in_admin_bar'  => false,
        'show_in_nav_menus'  => false,
        'can_export'         => true,
        'has_archive'        => false,   
        'exclude_from_search'=> true,
        'publicly_queryable' => false,
        'capability_type'    => 'post',
        'menu_position'      => 20,
        'supports'           => array( 'title' ),
      ) ) );
    }

    function spice_portfolio_shortcodes_meta_fn()
    {
      add_meta_box( 'spice_shortcode_meta_id', esc_html__( 'Portfolio Settings', 'spice-portfolio' ), array($this,'spice_portfolio_shortcode_meta_cb_fn'), 'portfolio_shortcodes','normal','high' );
      add_meta_box( 'spice_shortcode_generator_id', esc_html__( 'Portfolio  Shortcode', 'spice-portfolio' ), array($this,'spice_portfolio_shortcode_generator_fn'), 'portfolio_shortcodes','side','low' );
    }

    //Callback Meta Function
    function spice_portfolio_shortcode_meta_cb_fn()
    {
      require_once('spice-portfolio-shortcode-meta-box.php');
    }

    //Shortcode Generator Function
    function spice_portfolio_shortcode_generator_fn()
    {
      require_once('spice-portfolio-shortcode-generator.php');
    }

    //Save Meta Values
    function spice_portfolio_shortcode_meta_save($post_id) 
      {  
        if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || (defined('DOING_AJAX') && DOING_AJAX) || isset($_REQUEST['bulk_edit']))
              return;
          
        if ( ! current_user_can( 'edit_page', $post_id ) )
        {   return ;  } 
          
        if(isset( $_POST['post_ID']))
        {   
          $post_ID = absint($_POST['post_ID']);       
          $post_type=get_post_type($post_ID);
          
          if($post_type=='portfolio_shortcodes')
          { 
            update_post_meta($post_ID, 'sp_masonry_col', absint($_POST['sp_masonry_col']));
            update_post_meta($post_ID, 'sp_masonry_title_enable', sanitize_text_field($_POST['sp_masonry_title_enable']));
            update_post_meta($post_ID, 'sp_masonry_category_enable', sanitize_text_field($_POST['sp_masonry_category_enable']));
            update_post_meta($post_ID, 'sp_masonry_filter_enable', sanitize_text_field($_POST['sp_masonry_filter_enable']));
            update_post_meta($post_ID, 'sp_portfolio_orderby', sanitize_text_field($_POST['sp_portfolio_orderby']));
            update_post_meta($post_ID, 'spice_portfolio_post_page', absint($_POST['spice_portfolio_post_page']));
            update_post_meta($post_ID, 'sp_filter_font_family', sanitize_text_field($_POST['sp_filter_font_family']));
            update_post_meta($post_ID, 'sp_filter_font_size', absint($_POST['sp_filter_font_size']));
            update_post_meta($post_ID, 'sp_filter_lheight', absint($_POST['sp_filter_lheight']));
            update_post_meta($post_ID, 'sp_filter_font_wgt', absint($_POST['sp_filter_font_wgt']));
            update_post_meta($post_ID, 'sp_filter_fstyle', sanitize_text_field($_POST['sp_filter_fstyle']));
            update_post_meta($post_ID,'sp_filter_text_trans',sanitize_text_field($_POST['sp_filter_text_trans']));
            update_post_meta($post_ID,'sp_title_font_family',sanitize_text_field($_POST['sp_title_font_family']));
            update_post_meta($post_ID,'sp_title_font_size', absint($_POST['sp_title_font_size']));
            update_post_meta($post_ID,'sp_title_lheight', absint($_POST['sp_title_lheight']));
            update_post_meta($post_ID,'sp_title_font_wgt', absint($_POST['sp_title_font_wgt']));
            update_post_meta($post_ID,'sp_title_fstyle', sanitize_text_field($_POST['sp_title_fstyle']));
            update_post_meta($post_ID,'sp_title_text_trans', sanitize_text_field($_POST['sp_title_text_trans']));
            update_post_meta($post_ID,'sp_cat_font_family', sanitize_text_field($_POST['sp_cat_font_family']));
            update_post_meta($post_ID,'sp_cat_font_size', absint($_POST['sp_cat_font_size']));
            update_post_meta($post_ID,'sp_cat_lheight', absint($_POST['sp_cat_lheight']));
            update_post_meta($post_ID,'sp_cat_font_wgt', absint($_POST['sp_cat_font_wgt']));
            update_post_meta($post_ID,'sp_cat_fstyle', sanitize_text_field($_POST['sp_cat_fstyle']));
            update_post_meta($post_ID,'sp_cat_text_trans', sanitize_text_field($_POST['sp_cat_text_trans']));
            update_post_meta($post_ID,'sp_des_font_family', sanitize_text_field($_POST['sp_des_font_family']));
            update_post_meta($post_ID,'sp_des_font_size', absint($_POST['sp_des_font_size']));
            update_post_meta($post_ID,'sp_des_lheight', absint($_POST['sp_des_lheight']));
            update_post_meta($post_ID,'sp_des_font_wgt', absint($_POST['sp_des_font_wgt']));
            update_post_meta($post_ID,'sp_des_fstyle', sanitize_text_field($_POST['sp_des_fstyle']));
            update_post_meta($post_ID,'sp_des_text_trans', sanitize_text_field($_POST['sp_des_text_trans']));
            update_post_meta($post_ID, 'sp_overlay_clr', sanitize_hex_color($_POST['sp_overlay_clr']));
            update_post_meta($post_ID, 'sp_title_clr', sanitize_hex_color($_POST['sp_title_clr']));
            update_post_meta($post_ID, 'sp_title_hov_clr', sanitize_hex_color($_POST['sp_title_hov_clr']));
            update_post_meta($post_ID, 'sp_category_clr', sanitize_hex_color($_POST['sp_category_clr']));
            update_post_meta($post_ID, 'sp_category_hov_clr', sanitize_hex_color($_POST['sp_category_hov_clr']));
            update_post_meta($post_ID, 'sp_title_pop_clr', sanitize_hex_color($_POST['sp_title_pop_clr']));
            update_post_meta($post_ID, 'sp_des_clr', sanitize_hex_color($_POST['sp_des_clr']));
            update_post_meta($post_ID, 'sp_ftr_clr', sanitize_hex_color($_POST['sp_ftr_clr']));
            update_post_meta($post_ID, 'sp_ftr_hvr_clr', sanitize_hex_color($_POST['sp_ftr_hvr_clr']));
            update_post_meta($post_ID, 'sp_ftr_act_clr', sanitize_hex_color($_POST['sp_ftr_act_clr']));
            update_post_meta($post_ID, 'sp_ftr_bdr_clr', sanitize_hex_color($_POST['sp_ftr_bdr_clr']));
            
          // if(count($_POST['sp_cat_query'])==0) { $speak = ''; }
           if(empty($_POST['sp_cat_query'])) { $speak = ''; }
           else { $speak = sanitize_text_field(implode(',', $_POST['sp_cat_query'])); }
            
          update_post_meta($post_id, 'sp_cat_query', $speak);

          //if(count($_POST['sp_auth_query'])==0) { $sp_auth_query = ''; }
          if(empty($_POST['sp_auth_query'])) { $sp_auth_query = ''; }
          else { $sp_auth_query = sanitize_text_field(implode(',', $_POST['sp_auth_query'])); }
          update_post_meta($post_id, 'sp_auth_query', $sp_auth_query);             
          }     
        }       
      }

  }

}
new Spice_Portfolio_Shortcode_Post_Type;