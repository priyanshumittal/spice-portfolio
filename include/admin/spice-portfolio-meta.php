<?php
// Adding Meta box setting for spice_portfolio CPT
add_action('admin_init','spice_portfolio_plus_init');

// Adding Meta box setting callback function for spice_portfolio CPT
function spice_portfolio_plus_init()
	{	
		//Adds a meta box to one or more screens.
		add_meta_box('home_project_meta', esc_html__('Project Detail','spice-portfolio'), 'spice_portfolio_meta', 'spice_portfolio', 'normal', 'high');

		//The WordPress Core save post hook.
		add_action('save_post','spice_portfolio_plus_meta_save');
	}	
	
// Add meta box callback function for spice_portfolio CPT
function spice_portfolio_meta()
	{
		global $post ;
		$portfolio_link = get_post_meta( get_the_ID(), 'portfolio_link', true );
		$portfolio_target = get_post_meta( get_the_ID(), 'portfolio_target', true );
		$portfolio_title_description = get_post_meta( get_the_ID(), 'portfolio_title_description', true );?>	
		<p><h4 class="heading"><?php esc_attr_e('Link','spice-portfolio');?></h4></p>
		<p><input style="width:600px;" name="portfolio_link" id="portfolio_link" placeholder="<?php esc_attr_e('Link','spice-portfolio');?>" type="text" value="<?php if (!empty($portfolio_link)) echo esc_attr($portfolio_link);?>"> </p>
		<p><input type="checkbox" id="portfolio_target" name="portfolio_target" <?php if($portfolio_target) echo "checked"; ?> > <?php esc_attr_e('Open link in new tab','spice-portfolio'); ?></p>
		<p style="display: none;"><h4 class="heading"></h4>
		<p style="display: none;"><textarea name="portfolio_title_description" id="portfolio_title_description" style="width: 480px; height: 56px; padding: 0px;" placeholder="<?php esc_attr_e('Description','spice-portfolio');?>"  rows="3" cols="10" ><?php if (!empty($portfolio_title_description)) echo esc_attr($portfolio_title_description);?></textarea></p>	
<?php 
	}

//The WordPress Core save post hook callback function
function spice_portfolio_plus_meta_save($post_id) 
	{	 
		if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || (defined('DOING_AJAX') && DOING_AJAX) || isset($_REQUEST['bulk_edit']))
	        return;
			
		if ( ! current_user_can( 'edit_page', $post_id ) )
		{   return ;	} 
			
		if(isset( $_POST['post_ID']))
		{ 	
			$post_ID = absint($_POST['post_ID']);				
			$post_type=get_post_type($post_ID);
			
			if($post_type=='spice_portfolio')
			{	
				update_post_meta($post_ID, 'portfolio_title_description', sanitize_text_field($_POST['portfolio_title_description']));
				update_post_meta($post_ID, 'portfolio_link', sanitize_text_field($_POST['portfolio_link']));	
				update_post_meta($post_ID, 'portfolio_target', sanitize_text_field($_POST['portfolio_target']));
			}			
		}				
	}