<?php
/**
 * Register Post Type
 */
if ( ! class_exists( 'Spice_Portfolio_Post_Type' ) ) {

  class Spice_Portfolio_Post_Type
  {

    public function __construct()
    {
      //Register spice_portfolio CPT
      add_action('init', array( $this , 'spice_portfolio_register' ));
      
      if ( is_admin() ) {
        //Register category column for spice_portfolio CPT
        add_filter( 'manage_edit-spice_portfolio_columns', array( $this, 'spice_portfolio_columns' ) );
        add_action('manage_spice_portfolio_posts_custom_column', array( $this, 'spice_portfolio_manage_columns'), 10,2);

        //Register shortcode column for portfolio_shortcodes CPT
        add_filter( 'manage_edit-portfolio_shortcodes_columns', array( $this, 'spice_portfolio_shortcode_columns' ) );
        add_action('manage_portfolio_shortcodes_posts_custom_column',  array( $this, 'spice_portfolio_shortcodes_manage_columns'), 10, 2 );
      }
    }

    //Register spice_portfolio CPT callback function
    function spice_portfolio_register() {
      $labels = array(
        'name'               => esc_html__( 'Portfolio', 'spice-portfolio' ),
        'singular_name'      => esc_html__( 'Portfolio', 'spice-portfolio' ),
        'add_new'            => esc_html__( 'Add New', 'spice-portfolio' ),
        'add_new_item'       => esc_html__( 'Add New Portfolio', 'spice-portfolio'  ),
        'edit_item'          => esc_html__( 'Edit Portfolio', 'spice-portfolio'  ),
        'new_item'           => esc_html__( 'New Portfolio', 'spice-portfolio'  ),
        'all_items'          => esc_html__( 'All Portfolio', 'spice-portfolio'  ),
        'view_item'          => esc_html__( 'View Portfolio', 'spice-portfolio'  ),
        'search_items'       => esc_html__( 'Search Portfolio', 'spice-portfolio'  ),
        'not_found'          => esc_html__( 'No portfolios found', 'spice-portfolio'  ),
        'not_found_in_trash' => esc_html__( 'No portfolios found in the Trash', 'spice-portfolio'  ), 
        'parent_item_colon'  =>  '',
        'menu_name'          => 'Portfolio'
      );
      $args = array(
        'labels'        => $labels,
        'description'   => esc_html__( 'Holds our products and product specific data', 'spice-portfolio'  ),
        'public'        => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-portfolio',
        'supports'      => array( 'title', 'editor', 'thumbnail' , 'author'),
        'has_archive'   => false,
      );
    register_post_type( 'spice_portfolio', $args ); 
    }


    /**
     * Add categories column callback function
     *
     * @since 0.1
     */
    function spice_portfolio_columns( $columns ) {
      if ( taxonomy_exists( 'sp_portfolio_categories' ) ) {
        $columns['sp_portfolio_categories'] = esc_html__( 'Categories', 'spice-portfolio' );
      }
      
      return $columns;
    }


     /**
     * Shortcode column manage for portfolio_shortcodes CPT callback function
     *
     * @since 0.1
     */
    function spice_portfolio_manage_columns( $column, $post_id ) {

      switch ( $column ) :

        case 'sp_portfolio_categories':

          if ( $category_list = get_the_term_list( $post_id, 'sp_portfolio_categories', '', ', ', '' ) ) {
            echo $category_list;
          } else {
            echo '&mdash;';
          }

        break;


      endswitch;

    }


    /**
     * Shortcode column for portfolio_shortcodes CPT callback function
     *
     * @since 0.1
     */
    function spice_portfolio_shortcode_columns( $columns ) {
      $columns = array(
            'cb' => '<input type="checkbox" />',
            'title' => esc_html__( 'Shortcodes', 'spice-portfolio' ),
            'shortcode' => esc_html__( 'Shortcodes', 'spice-portfolio' ),
            'date' => esc_html__( 'Date', 'spice-portfolio' )
        );
        return $columns;
    }


    /**
     * Shortcode column manage for portfolio_shortcodes CPT callback function
     *
     * @since 0.1
     */
    function spice_portfolio_shortcodes_manage_columns( $column, $post_id ){
      global $post;
      switch( $column ) {
          case 'shortcode' :
            echo '<input style="width:225px" type="text" value="[spice_portfolio id='.$post_id.']" readonly="readonly" onclick="this.select()" />';
            break;
      
          default :
            break;
        }  
    }


  }

}

new Spice_Portfolio_Post_Type();