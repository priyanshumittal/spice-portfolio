<?php
//global $post ;
$spice_portfolio_post_page = get_post_meta( get_the_ID(), 'spice_portfolio_post_page', true );
$sp_masonry_col = get_post_meta( get_the_ID(), 'sp_masonry_col', true );
$sp_masonry_title_enable = get_post_meta( get_the_ID(), 'sp_masonry_title_enable', true );
$sp_masonry_category_enable = get_post_meta( get_the_ID(), 'sp_masonry_category_enable', true );
$sp_masonry_filter_enable = get_post_meta( get_the_ID(), 'sp_masonry_filter_enable', true );
$sp_portfolio_orderby = get_post_meta( get_the_ID(), 'sp_portfolio_orderby', true );
$sp_filter_font_family = get_post_meta( get_the_ID(), 'sp_filter_font_family', true );
$sp_filter_font_size = get_post_meta( get_the_ID(), 'sp_filter_font_size', true );
$sp_filter_lheight = get_post_meta( get_the_ID(), 'sp_filter_lheight', true );
$sp_filter_font_wgt = get_post_meta( get_the_ID(), 'sp_filter_font_wgt', true );
$sp_filter_fstyle = get_post_meta( get_the_ID(), 'sp_filter_fstyle', true );
$sp_filter_text_trans = get_post_meta( get_the_ID(), 'sp_filter_text_trans', true );
//Font Familty
$sp_title_font_family = get_post_meta( get_the_ID(), 'sp_title_font_family', true );
$sp_title_font_size = get_post_meta( get_the_ID(), 'sp_title_font_size', true );
$sp_title_lheight = get_post_meta( get_the_ID(), 'sp_title_lheight', true );
$sp_title_font_wgt = get_post_meta( get_the_ID(), 'sp_title_font_wgt', true );
$sp_title_fstyle = get_post_meta( get_the_ID(), 'sp_title_fstyle', true );
$sp_title_text_trans = get_post_meta( get_the_ID(), 'sp_title_text_trans', true );
$sp_cat_font_family = get_post_meta( get_the_ID(), 'sp_cat_font_family', true );
$sp_cat_font_size = get_post_meta( get_the_ID(), 'sp_cat_font_size', true );
$sp_cat_lheight = get_post_meta( get_the_ID(), 'sp_cat_lheight', true );
$sp_cat_font_wgt = get_post_meta( get_the_ID(), 'sp_cat_font_wgt', true );
$sp_cat_fstyle = get_post_meta( get_the_ID(), 'sp_cat_fstyle', true );
$sp_cat_text_trans = get_post_meta( get_the_ID(), 'sp_cat_text_trans', true );
$sp_des_font_family = get_post_meta( get_the_ID(), 'sp_des_font_family', true );
$sp_des_font_size = get_post_meta( get_the_ID(), 'sp_des_font_size', true );
$sp_des_lheight = get_post_meta( get_the_ID(), 'sp_des_lheight', true );
$sp_des_font_wgt = get_post_meta( get_the_ID(), 'sp_des_font_wgt', true );
$sp_des_fstyle = get_post_meta( get_the_ID(), 'sp_des_fstyle', true );
$sp_des_text_trans = get_post_meta( get_the_ID(), 'sp_des_text_trans', true );
//Color
$sp_overlay_clr=get_post_meta( get_the_ID(), 'sp_overlay_clr', true );
$sp_title_clr= get_post_meta( get_the_ID(), 'sp_title_clr', true );
$sp_title_hov_clr= get_post_meta( get_the_ID(), 'sp_title_hov_clr', true );
$sp_category_clr= get_post_meta( get_the_ID(), 'sp_category_clr', true );
$sp_category_hov_clr= get_post_meta( get_the_ID(), 'sp_category_hov_clr', true );
$sp_title_pop_clr= get_post_meta( get_the_ID(), 'sp_title_pop_clr', true );
$sp_des_clr= get_post_meta( get_the_ID(), 'sp_des_clr', true );
$sp_ftr_clr= get_post_meta( get_the_ID(), 'sp_ftr_clr', true );
$sp_ftr_hvr_clr= get_post_meta( get_the_ID(), 'sp_ftr_hvr_clr', true );
$sp_ftr_act_clr= get_post_meta( get_the_ID(), 'sp_ftr_act_clr', true );
$sp_ftr_bdr_clr= get_post_meta( get_the_ID(), 'sp_ftr_bdr_clr', true );
$query_tax='sp_portfolio_categories';
$query_term_args= array('hide_empty' => true, 'orderby' => 'id');
$query_tax_terms = get_terms($query_tax, $query_term_args);
$sp_cat_query=explode(',',get_post_meta(get_the_ID(),'sp_cat_query',true));
$sp_auth_query=explode(',',get_post_meta(get_the_ID(),'sp_auth_query',true));
$sp_font_size = array();
    for($i=9; $i<=100; $i++) {
      $sp_font_size[$i] = $i;
    }
$sp_line_height = array();
    for($i=1; $i<=100; $i++) {
        $sp_line_height[$i] = $i;
    }
$font_family = spice_portfolio_typo_fonts();
?>
<div id="tabs">
  <ul>
    <li><a href="#tabs-1"><?php echo esc_html__('General','spice-portfolio');?></a></li>
    <li><a href="#tabs-2"><?php echo esc_html__('Filter Bar','spice-portfolio');?></a></li>
    <li><a href="#tabs-3"><?php echo esc_html__('Color','spice-portfolio');?></a></li>
    <li><a href="#tabs-4"><?php echo esc_html__('Typography','spice-portfolio');?></a></li>
    <li><a href="#tabs-5"><?php echo esc_html__('Query','spice-portfolio');?></a></li>
  </ul>

  <div id="tabs-1">
    <table class="spice-portfolio-tbl">
      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Posts Per Page','spice-portfolio');?>
          <span class="spice-port-td-span"><?php echo esc_html__('Put the value to display portfolio items. Default value is 12.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr"><input class="spice-port-td-attr-width" min="1" step="1" type="number" name="spice_portfolio_post_page" value="<?php if (!empty($spice_portfolio_post_page)) echo esc_attr($spice_portfolio_post_page);?>" /></td>
      </tr>
      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Columns','spice-portfolio');?>
          <span class="spice-port-td-span"><?php echo esc_html__('Choose your columns number.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr">
            <div id="sp_masonry_col_id" class="size-slider" ></div>
              <input type="text" class="slider-text" id="sp_masonry_col" name="sp_masonry_col"  readonly="readonly">
        </td>
      </tr>
      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Title','spice-portfolio');?>
        <span class="spice-port-td-span"><?php echo esc_html__('Enable or disable the items title.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr">
          <div class="switch">
              <input type="radio" class="switch-input" name="sp_masonry_title_enable" value="yes" id="enable_masonry_title" <?php if($sp_masonry_title_enable == 'yes' ) { echo "checked"; } if($sp_masonry_title_enable == '' ) { echo "checked"; } ?> />
              <label for="enable_masonry_title" class="switch-label switch-label-off"><?php echo esc_html__('Yes','spice-portfolio'); ?></label>
              <input type="radio" class="switch-input" name="sp_masonry_title_enable" value="no" id="disable_masonry_title" <?php if($sp_masonry_title_enable == 'no' ) { echo "checked"; } ?> />
              <label for="disable_masonry_title" class="switch-label switch-label-on"><?php echo esc_html__('No','spice-portfolio'); ?></label>
              <span class="switch-selection"></span>
          </div>
        </td>
      </tr>
      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Category','spice-portfolio');?>
        <span class="spice-port-td-span"><?php echo esc_html__('Enable or disable the items category.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr">
          <div class="switch">
            <input type="radio" class="switch-input" name="sp_masonry_category_enable" value="yes" id="enable_masonry_category" <?php if($sp_masonry_category_enable == 'yes' ) { echo "checked"; } if($sp_masonry_category_enable == '' ) { echo "checked"; } ?> />
            <label for="enable_masonry_category" class="switch-label switch-label-off"><?php echo esc_html__('Yes','spice-portfolio'); ?></label>
            <input type="radio" class="switch-input" name="sp_masonry_category_enable" value="no" id="disable_masonry_category" <?php if($sp_masonry_category_enable == 'no' ) { echo "checked"; } ?> />
            <label for="disable_masonry_category" class="switch-label switch-label-on"><?php echo esc_html__('No','spice-portfolio'); ?></label>
            <span class="switch-selection"></span>
          </div>
        </td>
      </tr>
    </table>
  </div>
  <div id="tabs-2">
    <table class="spice-portfolio-tbl">
      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Filter','spice-portfolio');?>
        <span class="spice-port-td-span"><?php echo esc_html__('Enable or disable the filter bar.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr">
          <div class="switch">
            <input type="radio" class="switch-input" name="sp_masonry_filter_enable" value="yes" id="enable_masonry_filter" <?php if($sp_masonry_filter_enable == 'yes' ) { echo "checked"; } if($sp_masonry_filter_enable == '' ) { echo "checked"; } ?> />
            <label for="enable_masonry_filter" class="switch-label switch-label-off"><?php echo esc_html__('Yes','spice-portfolio'); ?></label>
            <input type="radio" class="switch-input" name="sp_masonry_filter_enable" value="no" id="disable_masonry_filter" <?php if($sp_masonry_filter_enable == 'no' ) { echo "checked"; } ?> />
            <label for="disable_masonry_filter" class="switch-label switch-label-on"><?php echo esc_html__('No','spice-portfolio'); ?></label>
            <span class="switch-selection"></span>
          </div>
        </td>
      </tr>
    </table>
  </div>
  <div id="tabs-3">
    <table class="spice-portfolio-tbl">
      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Filter Color','spice-portfolio');?>
        <span class="spice-port-td-span"><?php echo esc_html__('You can change filter color.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr">
          <input id="sp_ftr_clr" name="sp_ftr_clr" type="text" value="<?php if( empty($sp_ftr_clr) ) { echo '#858585'; } else {  echo esc_attr($sp_ftr_clr); } ?>" class="sp-color-field" data-default-color="#858585" />
         </td>
      </tr>
      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Filter Hover Color','spice-portfolio');?>
        <span class="spice-port-td-span"><?php echo esc_html__('You can change filter hover color.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr">
          <input id="sp_ftr_hvr_clr" name="sp_ftr_hvr_clr" type="text" value="<?php if( empty($sp_ftr_hvr_clr) ) { echo '#ff6f61'; } else {  echo esc_attr($sp_ftr_hvr_clr); } ?>" class="sp-color-field" data-default-color="#ff6f61" />
         </td>
      </tr>
      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Filter Active Color','spice-portfolio');?>
        <span class="spice-port-td-span"><?php echo esc_html__('You can change filter active color.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr">
          <input id="sp_ftr_act_clr" name="sp_ftr_act_clr" type="text" value="<?php if( empty($sp_ftr_act_clr) ) { echo '#ff6f61'; } else {  echo esc_attr($sp_ftr_act_clr); } ?>" class="sp-color-field" data-default-color="#ff6f61" />
         </td>
      </tr>
      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Filter Border Color','spice-portfolio');?>
        <span class="spice-port-td-span"><?php echo esc_html__('You can change filter border color.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr">
          <input id="sp_ftr_bdr_clr" name="sp_ftr_bdr_clr" type="text" value="<?php if( empty($sp_ftr_bdr_clr) ) { echo '#e1e1e1'; } else {  echo esc_attr($sp_ftr_bdr_clr); } ?>" class="sp-color-field" data-default-color="#e1e1e1" />
         </td>
      </tr>
      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Overlay Color','spice-portfolio');?>
        <span class="spice-port-td-span"><?php echo esc_html__('You can change overlay color.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr">
          <input id="sp_overlay_clr" name="sp_overlay_clr" type="text" value="<?php if( empty($sp_overlay_clr) ) { echo '#ff6f61'; } else {  echo esc_attr($sp_overlay_clr); } ?>" class="sp-color-field" data-default-color="#ff6f61" />
         </td>
      </tr>
      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Title Color','spice-portfolio');?>
        <span class="spice-port-td-span"><?php echo esc_html__('You can change title color.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr">
          <input id="sp_title_clr" name="sp_title_clr" type="text" value="<?php if( empty($sp_title_clr) ) { echo '#ffffff'; } else {  echo esc_attr($sp_title_clr); } ?>" class="sp-color-field" data-default-color="#ffffff" />
         </td>
      </tr>
      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Title Hover Color','spice-portfolio');?>
        <span class="spice-port-td-span"><?php echo esc_html__('You can change title hover color.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr">
          <input id="sp_title_hov_clr" name="sp_title_hov_clr" type="text" value="<?php if( empty($sp_title_hov_clr) ) { echo '#000000'; } else {  echo esc_attr($sp_title_hov_clr); } ?>" class="sp-color-field" data-default-color="#000000" />
         </td>
      </tr>
      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Category Color','spice-portfolio');?>
        <span class="spice-port-td-span"><?php echo esc_html__('You can change category color.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr">
          <input id="sp_category_clr" name="sp_category_clr" type="text" value="<?php if( empty($sp_category_clr) ) { echo '#ffffff'; } else {  echo esc_attr($sp_category_clr); } ?>" class="sp-color-field" data-default-color="#ffffff" />
         </td>
      </tr>
      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Category Hover Color','spice-portfolio');?>
        <span class="spice-port-td-span"><?php echo esc_html__('You can change category hover color.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr">
          <input id="sp_category_hov_clr" name="sp_category_hov_clr" type="text" value="<?php if( empty($sp_category_hov_clr) ) { echo '#000000'; } else {  echo esc_attr($sp_category_hov_clr); } ?>" class="sp-color-field" data-default-color="#000000" />
         </td>
      </tr>
      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Title (Pop Up) Color','spice-portfolio');?>
        <span class="spice-port-td-span"><?php echo esc_html__('You can change title (Pop Up) color.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr">
          <input id="sp_title_pop_clr" name="sp_title_pop_clr" type="text" value="<?php if( empty($sp_title_pop_clr) ) { echo '#000000'; } else {  echo esc_attr($sp_title_pop_clr); } ?>" class="sp-color-field" data-default-color="#000000" />
         </td>
      </tr>
      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Description (Pop Up) Color','spice-portfolio');?>
        <span class="spice-port-td-span"><?php echo esc_html__('You can change description (Pop Up) color.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr">
          <input id="sp_des_clr" name="sp_des_clr" type="text" value="<?php if( empty($sp_des_clr) ) { echo '#858585'; } else {  echo esc_attr($sp_des_clr); } ?>" class="sp-color-field" data-default-color="#858585" />
         </td>
      </tr>
    </table>
  </div>
   <div id="tabs-4">
    <!-- Filter Typography -->
    <table class="spice-portfolio-tbl">
      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Filter Typography','spice-portfolio');?>
        <span class="spice-port-td-span"><?php echo esc_html__('Typography for the filter.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Font Family','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_filter_font_family" style="display:block;">
            <?php
            foreach ($font_family as $font_key => $font_value) { ?>
              <option value="<?php echo esc_attr($font_key);?>" <?php if($sp_filter_font_family == $font_key) { echo "selected"; } if(empty($sp_filter_font_family) && ($font_key=='Poppins')) { echo "selected"; } ?> ><?php echo esc_html($font_value);?></option>
              
            <?php }  ?>           
          </select>
        </td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Font Size in px','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_filter_font_size" style="display:block;">
            <?php
            foreach ($sp_font_size as $sp_font_size_key => $sp_font_size_value) { ?>
              <option value="<?php echo esc_attr($sp_font_size_key);?>" <?php if($sp_filter_font_size == $sp_font_size_key) { echo "selected"; } else { if((empty($sp_filter_font_size) && ($sp_font_size_key==20))) {  echo "selected"; } } ?> ><?php echo esc_html($sp_font_size_value);?></option>
              
            <?php }  ?>           
          </select>
        </td>
      </tr>

      <tr>
        <td class="spice-port-td-title"></td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Line Height in px','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_filter_lheight" style="display:block;">
            <?php
            foreach ($sp_line_height as $sp_line_height_key => $sp_line_height_value) { ?>
              <option value="<?php echo esc_attr($sp_line_height_key);?>" <?php if($sp_filter_lheight == $sp_line_height_key) { echo "selected"; } else { if( (empty($sp_filter_lheight) && ($sp_line_height_key==32))) {  echo "selected"; } } ?> ><?php echo esc_html($sp_line_height_value);?></option>
            <?php }  ?>           
          </select>
        </td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Font Weight','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_filter_font_wgt" style="display:block;">
            <option value="100" <?php if($sp_filter_font_wgt == 100) { echo "selected"; } ?> ><?php echo esc_html__('100','spice-portfolio');?></option>   
            <option value="200" <?php if($sp_filter_font_wgt == 200) { echo "selected"; } ?> ><?php echo esc_html__('200','spice-portfolio');?></option>
            <option value="300" <?php if($sp_filter_font_wgt == 300) { echo "selected"; } ?> ><?php echo esc_html__('300','spice-portfolio');?></option>
            <option value="400" <?php if($sp_filter_font_wgt == 400) { echo "selected"; } if(empty($sp_filter_font_wgt)) { echo "selected"; } ?> ><?php echo esc_html__('400','spice-portfolio');?></option>
            <option value="500" <?php if($sp_filter_font_wgt == 500) { echo "selected"; } ?> ><?php echo esc_html__('500','spice-portfolio');?></option>
            <option value="600" <?php if($sp_filter_font_wgt == 600) { echo "selected"; } ?> ><?php echo esc_html__('600','spice-portfolio');?></option>
            <option value="700" <?php if($sp_filter_font_wgt == 700) { echo "selected"; } ?> ><?php echo esc_html__('700','spice-portfolio');?></option>
            <option value="800" <?php if($sp_filter_font_wgt == 800) { echo "selected"; } ?> ><?php echo esc_html__('800','spice-portfolio');?></option>
            <option value="900" <?php if($sp_filter_font_wgt == 900) { echo "selected"; } ?> ><?php echo esc_html__('900','spice-portfolio');?></option>       
          </select>
        </td>
      </tr>

      <tr>
        <td class="spice-port-td-title"></td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Font style','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_filter_fstyle" style="display:block;">
            <option value="normal" <?php if($sp_filter_fstyle == 'normal') { echo "selected"; } ?> ><?php echo esc_html__('Normal','spice-portfolio');?></option>   
            <option value="italic" <?php if($sp_filter_fstyle == 'italic') { echo "selected"; } ?> ><?php echo esc_html__('Italic','spice-portfolio');?></option>          
          </select>
        </td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Text Transform','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_filter_text_trans" style="display:block;">
            <option value="initial" <?php if($sp_filter_text_trans == 'initial') { echo "selected"; } ?>><?php echo esc_html__('Default','spice-portfolio');?></option>   
            <option value="capitalize" <?php if($sp_filter_text_trans == 'capitalize') { echo "selected"; } ?>><?php echo esc_html__('Capitalize','spice-portfolio');?></option> 
            <option value="lowercase" <?php if($sp_filter_text_trans == 'lowercase') { echo "selected"; } ?>><?php echo esc_html__('Lowercase','spice-portfolio');?></option> 
            <option value="Uppercase" <?php if($sp_filter_text_trans == 'Uppercase') { echo "selected"; } ?>><?php echo esc_html__('Uppercase','spice-portfolio');?></option>          
          </select>
        </td>
      </tr>
    </table>
  <hr>
    <!-- Portfolio Title Typography -->
    <table class="spice-portfolio-tbl">
      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Title Typography','spice-portfolio');?>
        <span class="spice-port-td-span"><?php echo esc_html__('Typography for the Title.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Font Family','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_title_font_family" style="display:block;">
            <?php
            foreach ($font_family as $font_key => $font_value) { ?>
              <option value="<?php echo esc_attr($font_key);?>" <?php if($sp_title_font_family == $font_key) { echo "selected"; } if(empty($sp_title_font_family) && ($font_key=='Poppins')) { echo "selected"; } ?> ><?php echo esc_html($font_value);?></option>

            <?php }  ?>           
          </select>
        </td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Font Size in px','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_title_font_size" style="display:block;">
            <?php
            foreach ($sp_font_size as $sp_font_size_key => $sp_font_size_value) { ?>
              <option value="<?php echo esc_attr($sp_font_size_key);?>" <?php if($sp_title_font_size == $sp_font_size_key) { echo "selected"; } else { if( (empty($sp_title_font_size) && ($sp_font_size_key==24))) {  echo "selected"; } } ?> ><?php echo esc_html($sp_font_size_value);?></option>
              
            <?php }  ?>           
          </select>
        </td>
      </tr>

      <tr>
        <td class="spice-port-td-title"></td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Line Height in px','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_title_lheight" style="display:block;">
            <?php
            foreach ($sp_line_height as $sp_line_height_key => $sp_line_height_value) { ?>
              <option value="<?php echo esc_attr($sp_line_height_key);?>" <?php if($sp_title_lheight == $sp_line_height_key) { echo "selected"; } else { if( (empty($sp_title_lheight) && ($sp_line_height_key==38))) {  echo "selected"; } } ?> ><?php echo esc_html($sp_line_height_value);?></option>
              
            <?php }  ?>           
          </select>
        </td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Font Weight','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_title_font_wgt" style="display:block;">
            <option value="100" <?php if($sp_title_font_wgt == 100) { echo "selected"; } ?> ><?php echo esc_html__('100','spice-portfolio');?></option>   
            <option value="200" <?php if($sp_title_font_wgt == 200) { echo "selected"; } ?> ><?php echo esc_html__('200','spice-portfolio');?></option>
            <option value="300" <?php if($sp_title_font_wgt == 300) { echo "selected"; } ?> ><?php echo esc_html__('300','spice-portfolio');?></option>
            <option value="400" <?php if($sp_title_font_wgt == 400) { echo "selected"; } ?> ><?php echo esc_html__('400','spice-portfolio');?></option>
            <option value="500" <?php if($sp_title_font_wgt == 500) { echo "selected"; } ?> ><?php echo esc_html__('500','spice-portfolio');?></option>
            <option value="600" <?php if($sp_title_font_wgt == 600) { echo "selected"; }  ?> ><?php echo esc_html__('600','spice-portfolio');?></option>
            <option value="700" <?php if($sp_title_font_wgt == 700) { echo "selected"; } if(empty($sp_title_font_wgt)) { echo "selected"; } ?> ><?php echo esc_html__('700','spice-portfolio');?></option>
            <option value="800" <?php if($sp_title_font_wgt == 800) { echo "selected"; } ?> ><?php echo esc_html__('800','spice-portfolio');?></option>
            <option value="900" <?php if($sp_title_font_wgt == 900) { echo "selected"; } ?> ><?php echo esc_html__('900','spice-portfolio');?></option>       
          </select>
        </td>
      </tr>

      <tr>
        <td class="spice-port-td-title"></td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Font style','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_title_fstyle" style="display:block;">
            <option value="normal" <?php if($sp_title_fstyle == 'normal') { echo "selected"; } ?>><?php echo esc_html__('Normal','spice-portfolio');?></option>   
            <option value="italic" <?php if($sp_title_fstyle == 'italic') { echo "selected"; } ?>><?php echo esc_html__('Italic','spice-portfolio');?></option>          
          </select>
        </td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Text Transform','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_title_text_trans" style="display:block;">
            <option value="initial" <?php if($sp_title_text_trans == 'initial') { echo "selected"; } ?>><?php echo esc_html__('Default','spice-portfolio');?></option>   
            <option value="capitalize" <?php if($sp_title_text_trans == 'capitalize') { echo "selected"; } ?>><?php echo esc_html__('Capitalize','spice-portfolio');?></option> 
            <option value="lowercase" <?php if($sp_title_text_trans == 'lowercase') { echo "selected"; } ?>><?php echo esc_html__('Lowercase','spice-portfolio');?></option> 
            <option value="Uppercase" <?php if($sp_title_text_trans == 'Uppercase') { echo "selected"; } ?>><?php echo esc_html__('Uppercase','spice-portfolio');?></option>          
          </select>
        </td>
      </tr>
    </table>
<hr> 
    <!-- Portfolio Category Typography -->
    <table class="spice-portfolio-tbl">
      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Category Typography','spice-portfolio');?>
        <span class="spice-port-td-span"><?php echo esc_html__('Typography for the Category.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Font Family','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_cat_font_family" style="display:block;">
            <?php
            foreach ($font_family as $font_key => $font_value) { ?>
              <option value="<?php echo esc_attr($font_key);?>" <?php if($sp_cat_font_family == $font_key) { echo "selected"; } if(empty($sp_cat_font_family) && ($font_key=='Poppins')) { echo "selected"; } ?> ><?php echo esc_html($font_value);?></option>
              
            <?php }  ?>           
          </select>
        </td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Font Size in px','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_cat_font_size" style="display:block;">
            <?php
            foreach ($sp_font_size as $sp_font_size_key => $sp_font_size_value) { ?>
              <option value="<?php echo esc_attr($sp_font_size_key);?>" <?php if($sp_cat_font_size == $sp_font_size_key) { echo "selected"; } else { if($sp_font_size_key==16) {  echo "selected"; } } ?> ><?php echo esc_html($sp_font_size_value);?></option>
              
            <?php }  ?>           
          </select>
        </td>
      </tr>

      <tr>
        <td class="spice-port-td-title"></td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Line Height in px','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_cat_lheight" style="display:block;">
            <?php
            foreach ($sp_line_height as $sp_line_height_key => $sp_line_height_value) { ?>
              <option value="<?php echo esc_attr($sp_line_height_key);?>" <?php if($sp_cat_lheight == $sp_line_height_key) { echo "selected"; } else { if($sp_line_height_key==26) {  echo "selected"; } } ?> ><?php echo esc_html($sp_line_height_value);?></option>
              
            <?php }  ?>           
          </select>
        </td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Font Weight','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_cat_font_wgt" style="display:block;">
            <option value="100" <?php if($sp_cat_font_wgt == 100) { echo "selected"; } ?> ><?php echo esc_html__('100','spice-portfolio');?></option>   
            <option value="200" <?php if($sp_cat_font_wgt == 200) { echo "selected"; } ?> ><?php echo esc_html__('200','spice-portfolio');?></option>
            <option value="300" <?php if($sp_cat_font_wgt == 300) { echo "selected"; } ?> ><?php echo esc_html__('300','spice-portfolio');?></option>
            <option value="400" <?php if($sp_cat_font_wgt == 400) { echo "selected"; } if(empty($sp_cat_font_wgt)) { echo "selected"; } ?> ><?php echo esc_html__('400','spice-portfolio');?></option>
            <option value="500" <?php if($sp_cat_font_wgt == 500) { echo "selected"; } ?> ><?php echo esc_html__('500','spice-portfolio');?></option>
            <option value="600" <?php if($sp_cat_font_wgt == 600) { echo "selected"; }  ?> ><?php echo esc_html__('600','spice-portfolio');?></option>
            <option value="700" <?php if($sp_cat_font_wgt == 700) { echo "selected"; } ?> ><?php echo esc_html__('700','spice-portfolio');?></option>
            <option value="800" <?php if($sp_cat_font_wgt == 800) { echo "selected"; } ?> ><?php echo esc_html__('800','spice-portfolio');?></option>
            <option value="900" <?php if($sp_cat_font_wgt == 900) { echo "selected"; } ?> ><?php echo esc_html__('900','spice-portfolio');?></option>       
          </select>
        </td>
      </tr>

      <tr>
        <td class="spice-port-td-title"></td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Font style','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_cat_fstyle" style="display:block;">
            <option value="normal" <?php if($sp_cat_fstyle == 'normal') { echo "selected"; } ?>><?php echo esc_html__('Normal','spice-portfolio');?></option>   
            <option value="italic" <?php if($sp_cat_fstyle == 'italic') { echo "selected"; } ?>><?php echo esc_html__('Italic','spice-portfolio');?></option>          
          </select>
        </td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Text Transform','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_cat_text_trans" style="display:block;">
            <option value="initial" <?php if($sp_cat_text_trans == 'initial') { echo "selected"; } ?>><?php echo esc_html__('Default','spice-portfolio');?></option>   
            <option value="capitalize" <?php if($sp_cat_text_trans == 'capitalize') { echo "selected"; } ?>><?php echo esc_html__('Capitalize','spice-portfolio');?></option> 
            <option value="lowercase" <?php if($sp_cat_text_trans == 'lowercase') { echo "selected"; } ?>><?php echo esc_html__('Lowercase','spice-portfolio');?></option> 
            <option value="Uppercase" <?php if($sp_cat_text_trans == 'Uppercase') { echo "selected"; } ?>><?php echo esc_html__('Uppercase','spice-portfolio');?></option>          
          </select>
        </td>
      </tr>
    </table>   
<hr>   
<!-- Portfolio Description Typography -->
    <table class="spice-portfolio-tbl">
      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Description Typography','spice-portfolio');?>
        <span class="spice-port-td-span"><?php echo esc_html__('Typography for the Description.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Font Family','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_des_font_family" style="display:block;">
            <?php
            foreach ($font_family as $font_key => $font_value) { ?>
              <option value="<?php echo $font_key;?>" <?php if($sp_des_font_family == $font_key) { echo "selected"; } if(empty($sp_des_font_family) && ($font_key=='Poppins')) { echo "selected"; } ?> ><?php echo $font_value;?></option>
              
            <?php }  ?>           
          </select>
        </td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Font Size in px','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_des_font_size" style="display:block;">
            <?php
            foreach ($sp_font_size as $sp_font_size_key => $sp_font_size_value) { ?>
              <option value="<?php echo esc_attr($sp_font_size_key);?>" <?php if($sp_des_font_size == $sp_font_size_key) { echo "selected"; } else { if($sp_font_size_key==20) {  echo "selected"; } } ?> ><?php echo esc_html($sp_font_size_value);?></option>
              
            <?php }  ?>           
          </select>
        </td>
      </tr>

      <tr>
        <td class="spice-port-td-title"></td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Line Height in px','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_des_lheight" style="display:block;">
            <?php
            foreach ($sp_line_height as $sp_line_height_key => $sp_line_height_value) { ?>
              <option value="<?php echo esc_attr($sp_line_height_key);?>" <?php if($sp_des_lheight == $sp_line_height_key) { echo "selected"; } else { if($sp_line_height_key==32) {  echo "selected"; } } ?> ><?php echo esc_html($sp_line_height_value);?></option>
              
            <?php }  ?>           
          </select>
        </td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Font Weight','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_des_font_wgt" style="display:block;">
            <option value="100" <?php if($sp_des_font_wgt == 100) { echo "selected"; } ?> ><?php echo esc_html__('100','spice-portfolio');?></option>   
            <option value="200" <?php if($sp_des_font_wgt == 200) { echo "selected"; } ?> ><?php echo esc_html__('200','spice-portfolio');?></option>
            <option value="300" <?php if($sp_des_font_wgt == 300) { echo "selected"; } ?> ><?php echo esc_html__('300','spice-portfolio');?></option>
            <option value="400" <?php if($sp_des_font_wgt == 400) { echo "selected"; } if(empty($sp_des_font_wgt)) { echo "selected"; } ?> ><?php echo esc_html__('400','spice-portfolio');?></option>
            <option value="500" <?php if($sp_des_font_wgt == 500) { echo "selected"; } ?> ><?php echo esc_html__('500','spice-portfolio');?></option>
            <option value="600" <?php if($sp_des_font_wgt == 600) { echo "selected"; } ?> ><?php echo esc_html__('600','spice-portfolio');?></option>
            <option value="700" <?php if($sp_des_font_wgt == 700) { echo "selected"; } ?> ><?php echo esc_html__('700','spice-portfolio');?></option>
            <option value="800" <?php if($sp_des_font_wgt == 800) { echo "selected"; } ?> ><?php echo esc_html__('800','spice-portfolio');?></option>
            <option value="900" <?php if($sp_des_font_wgt == 900) { echo "selected"; } ?> ><?php echo esc_html__('900','spice-portfolio');?></option>       
          </select>
        </td>
      </tr>

      <tr>
        <td class="spice-port-td-title"></td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Font style','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_des_fstyle" style="display:block;">
            <option value="normal" <?php if($sp_des_fstyle == 'normal') { echo "selected"; } ?>><?php echo esc_html__('Normal','spice-portfolio');?></option>   
            <option value="italic" <?php if($sp_des_fstyle == 'italic') { echo "selected"; } ?>><?php echo esc_html__('Italic','spice-portfolio');?></option>          
          </select>
        </td>
        <td class="spice-port-td-attr-30">
          <?php echo esc_html__('Text Transform','spice-portfolio');?>
          <select class="spice-portfolio-font-select" name="sp_des_text_trans" style="display:block;">
            <option value="initial" <?php if($sp_des_text_trans == 'initial') { echo "selected"; } ?>><?php echo esc_html__('Default','spice-portfolio');?></option>   
            <option value="capitalize" <?php if($sp_des_text_trans == 'capitalize') { echo "selected"; } ?>><?php echo esc_html__('Capitalize','spice-portfolio');?></option> 
            <option value="lowercase" <?php if($sp_des_text_trans == 'lowercase') { echo "selected"; } ?>><?php echo esc_html__('Lowercase','spice-portfolio');?></option> 
            <option value="Uppercase" <?php if($sp_des_text_trans == 'Uppercase') { echo "selected"; } ?>><?php echo esc_html__('Uppercase','spice-portfolio');?></option>          
          </select>
        </td>
      </tr>
    </table>  
  </div>
   <div id="tabs-5">
    <table class="spice-portfolio-tbl">
      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Categories','spice-portfolio');?>
        <span class="spice-port-td-span"><?php echo esc_html__('Display items by categories.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr">
         <select class="sp-portforlio-cat-query" name="sp_cat_query[]" multiple="multiple">
          <?php if ($query_tax_terms) :
            foreach ($query_tax_terms as $query_tax_term) { ?>
          <option value="<?php echo esc_attr($query_tax_term->slug);?>" <?php if (in_array($query_tax_term->slug, $sp_cat_query)) { echo "selected"; } ?>><?php echo esc_html($query_tax_term->name);?></option>
           <?php } endif; ?>
          </select></td>
      </tr>

      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Author','spice-portfolio');?>
        <span class="spice-port-td-span"><?php echo esc_html__('Display items by author.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr">
         <select class="sp-portforlio-auth-query" name="sp_auth_query[]" multiple="multiple">
          <?php
          $sp_author = get_users();
            foreach ( $sp_author as $sp_authors ) { ?>
            <option value="<?php echo esc_attr($sp_authors->ID);?>" <?php if (in_array($sp_authors->ID, $sp_auth_query)) { echo "selected"; } ?>><?php echo esc_html($sp_authors->user_nicename);?></option>
           <?php }  ?>
          </select></td>
      </tr>

      <tr>
        <td class="spice-port-td-title"><?php echo esc_html__('Order','spice-portfolio');?>
        <span class="spice-port-td-span"><?php echo esc_html__('Designates the ascending or descending order.','spice-portfolio');?></span></td>
        <td class="spice-port-td-attr">
         <select class="sp-portforlio-orderby" name="sp_portfolio_orderby">
            <option value="ASC" <?php if($sp_portfolio_orderby=='ASC') { echo "selected"; } ?> ><?php echo esc_html__('Ascending Order','spice-portfolio');?></option>
            <option value="DESC" <?php if($sp_portfolio_orderby=='DESC') { echo "selected"; } ?>><?php echo esc_html__('Descending Order','spice-portfolio');?></option>
          </select></td>
      </tr>
    </table>

  </div>
</div>

<script>
 //Team member description font slider size script
  jQuery(function() {
    jQuery( "#sp_masonry_col_id" ).slider({
    orientation: "horizontal",
    range: "min",
    default:2,
    max: 4,
    min:2,
    slide: function( event, ui ) {
    jQuery( "#sp_masonry_col" ).val( ui.value );
      }
    });
    
    jQuery( "#sp_masonry_col_id" ).slider("value",<?php echo intval($sp_masonry_col); ?> );
    jQuery( "#sp_masonry_col" ).val( jQuery( "#sp_masonry_col_id" ).slider( "value") );
    
  });
</script>