<?php 
    if(empty($sp_auth_query) && empty($sp_cat_query) )
    {
      $args = array('post_type' => $post_type,'post_status' => 'publish', 'posts_per_page' => $posts_per_page,'paged' => $curpage,'order' => $sp_portfolio_orderby);
    }

    else if(empty($sp_auth_query) || empty($sp_cat_query) )
    {
      if(empty($sp_auth_query))
      { 
        $args = array('post_type' => $post_type,'post_status' => 'publish','sp_portfolio_categories' => $sp_cat_arr, 'posts_per_page' => $posts_per_page,'paged' => $curpage,'order' => $sp_portfolio_orderby);  
      }
      else
      { 
        $args = array('post_type' => $post_type,'post_status' => 'publish', 'author__in' => $sp_auth_arr, 'posts_per_page' => $posts_per_page,'paged' => $curpage,'order' => $sp_portfolio_orderby);
      }
    }

    else if(!empty($sp_auth_query) && !empty($sp_cat_query) )
    {
    $args = array('post_type' => $post_type,'post_status' => 'publish','sp_portfolio_categories' => $sp_cat_arr, 'author__in' => $sp_auth_arr, 'posts_per_page' => $posts_per_page,'paged' => $curpage,'order' => $sp_portfolio_orderby);
    }
?>
<section class="page-section-space sps-portfolio bg-default port<?php echo intval($primary_key);?>">
  <div class="portfolio-container">
    <!-- Grid row -->
    <div class="tab-content filtering fil<?php echo intval($primary_key);?>">
      <!--Panel 1-->
      <?php   
      $portfolio_query = new WP_Query($args);
      if ($portfolio_query->have_posts()): ?>
      <div class="portfolio-row gallery gall<?php echo intval($primary_key);?>">
         <?php
         $inc=1;
         while ($portfolio_query->have_posts()) : $portfolio_query->the_post(); ?>
        <div class="item-<?php echo esc_attr($sp_column);?>column <?php echo esc_attr($inc);?> <?php echo esc_attr($primary_key.'_'.$inc);?>">
          <div class="portDetail">
            <figure class="portfolio-thumbnail">
              <?php
              the_post_thumbnail('full', array('class' => 'card-img-top','alt'   => get_the_title() ) );
                  $portfolio_target = sanitize_text_field(get_post_meta(get_the_ID(), 'portfolio_target', true));
                  $tagt = (!empty($portfolio_target)) ? 'target="_blank"' : '';
                  if(get_post_meta( get_the_ID(), 'portfolio_link', true ))
                  {
                    $port_link = get_post_meta( get_the_ID(), 'portfolio_link', true );
                  }
                  else
                  {
                    $port_link='';
                  }                  
                ?>
              <div class="portfolio-item-description">
                <div class="portfolio_description">
                  <?php if($sp_category=='yes'): ?>
                  <div class="portfolio-item-meta">
                    <h5 class="post_cats">
                      <?php //echo  the_taxonomies(array('template' => '% %l'));
                      $sp_taxo=get_the_taxonomies();
                      echo substr(str_replace('Categories:', '', $sp_taxo['sp_portfolio_categories']),0,-1);
                      ?>
                    </h5>
                  </div>
                  <?php endif;
                  if($sp_title=='yes'):?>
                  <div class="portfolio-item-title">
                    <h4 class="title">
                      <?php if(!empty($port_link)) { ?>
                            <a href="<?php echo esc_url($port_link);?>" <?php echo esc_attr($tagt);?>><?php the_title();?></a>
                        <?php }else {
                            the_title();
                        }?>
                    </h4>
                  </div>
                  <?php endif;?>
                </div>
                <div class="see-more">
                  <a role="button" id="clickBtn~<?php echo esc_attr($primary_key.'_'.$inc);?>" class="sp_view_btn">
                    <i class="fa fa-plus"></i>
                  </a>
                </div>
              </div>
              <div class="overlay"></div>
            </figure>
            <!-- Card -->
          </div>
        </div>
        <?php
        $inc++;
         endwhile; ?>
      </div>
      <?php endif;?>
    </div>
  </div>
  <!--Popup content-->
  <?php
  $inc=1;
  $portfolio_query = new WP_Query($args);
  if ($portfolio_query->have_posts()):
  while ($portfolio_query->have_posts()) : $portfolio_query->the_post();?>
  <div id="portData<?php echo esc_attr($primary_key.'_'.$inc);?>" class="portModal">
    <article class="port-modal-content">
      <span class="portClose" id="portcls<?php echo esc_attr($primary_key.'_'.$inc);?>">&times;</span>
      <figure class="post-thumbnail">
        <?php the_post_thumbnail('full', array('class' => 'card-img-top','alt'   => get_the_title() ) );?>
      </figure>
      <div class="port-content">
        <header class="entry-header"><h3 class="entry-title"><?php the_title();?></h3></header>
        <div class="entry-content"><?php the_content();?></div>
      </div>
    </article>
  </div>
  <?php 
  $inc++;
  endwhile;
  wp_reset_query();
  endif;
  ?>
</section>
<script type="text/javascript" id="#<?php echo esc_attr($primary_key.'_'.$inc);?>">
    jQuery('.sp_view_btn').click(function(){
      var split_id=jQuery(this).attr('id');
      var split_str = split_id.split('~');
      var sp_str = split_str[1];
      var modalData = document.getElementById('portData'+sp_str);
      var clickId = document.getElementById('clickBtn~'+sp_str);
      var clsclick = document.getElementById('portcls'+sp_str);

      modalData.style.display = "block";
      clsclick.onclick = function() 
      {
          modalData.style.display = "none";
      }
      window.onclick = function(event) 
      {
        if (event.target == modalData) {
              modalData.style.display = "none";
        }
      }

    });
</script>