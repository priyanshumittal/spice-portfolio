<?php
// Exit if accessed directly
if( ! defined('ABSPATH'))
{
	die('Do not open this file directly.');
}

add_shortcode( 'spice_portfolio', 'sp_shortcode' );
function sp_shortcode( $id )
{
	ob_start();
	global $primary_key;
	global $paged;
	$curpage = $paged ? $paged : 1;
	$primary_key= !isset($id['id']) ? $portfolio_shortcodes_cpt_id='' : $portfolio_shortcodes_cpt_id=$id['id'];
	$sp_counts=get_post_meta($portfolio_shortcodes_cpt_id,'spice_portfolio_post_page', true );
	$sp_counts=($sp_counts<1) ? 12: $sp_counts;
	$sp_column=get_post_meta($portfolio_shortcodes_cpt_id,'sp_masonry_col', true );
	$sp_title=get_post_meta($portfolio_shortcodes_cpt_id, 'sp_masonry_title_enable',true);
	$sp_category=get_post_meta($portfolio_shortcodes_cpt_id,'sp_masonry_category_enable',true);
	$sp_filter=get_post_meta($portfolio_shortcodes_cpt_id,'sp_masonry_filter_enable',true);
	$sp_cat_query=get_post_meta($portfolio_shortcodes_cpt_id,'sp_cat_query',true);
	$sp_filter_font_family=get_post_meta($portfolio_shortcodes_cpt_id,'sp_filter_font_family',true);
	$sp_filter_font_size=get_post_meta($portfolio_shortcodes_cpt_id,'sp_filter_font_size',true);
	$sp_filter_lheight=get_post_meta($portfolio_shortcodes_cpt_id,'sp_filter_lheight',true);
	$sp_filter_font_wgt=get_post_meta($portfolio_shortcodes_cpt_id,'sp_filter_font_wgt',true);
	$sp_filter_fstyle=get_post_meta($portfolio_shortcodes_cpt_id,'sp_filter_fstyle',true);
	$sp_filter_text_trans=get_post_meta($portfolio_shortcodes_cpt_id,'sp_filter_text_trans',true);
	$sp_title_font_family=get_post_meta($portfolio_shortcodes_cpt_id,'sp_title_font_family',true);
	$sp_title_font_size=get_post_meta($portfolio_shortcodes_cpt_id,'sp_title_font_size',true);
	$sp_title_lheight=get_post_meta($portfolio_shortcodes_cpt_id,'sp_title_lheight',true);
	$sp_title_font_wgt=get_post_meta($portfolio_shortcodes_cpt_id,'sp_title_font_wgt',true);
	$sp_title_fstyle=get_post_meta($portfolio_shortcodes_cpt_id,'sp_title_fstyle',true);
	$sp_title_text_trans=get_post_meta($portfolio_shortcodes_cpt_id,'sp_title_text_trans',true);
	$sp_cat_font_family=get_post_meta($portfolio_shortcodes_cpt_id,'sp_cat_font_family',true);
	$sp_cat_font_size=get_post_meta($portfolio_shortcodes_cpt_id,'sp_cat_font_size',true);
	$sp_cat_lheight=get_post_meta($portfolio_shortcodes_cpt_id,'sp_cat_lheight',true);
	$sp_cat_font_wgt=get_post_meta($portfolio_shortcodes_cpt_id,'sp_cat_font_wgt',true);
	$sp_cat_fstyle=get_post_meta($portfolio_shortcodes_cpt_id,'sp_cat_fstyle',true);
	$sp_cat_text_trans=get_post_meta($portfolio_shortcodes_cpt_id,'sp_cat_text_trans',true);
	$sp_des_font_family=get_post_meta($portfolio_shortcodes_cpt_id,'sp_des_font_family',true);
	$sp_des_font_size=get_post_meta($portfolio_shortcodes_cpt_id,'sp_des_font_size',true);
	$sp_des_lheight=get_post_meta($portfolio_shortcodes_cpt_id,'sp_des_lheight',true);
	$sp_des_font_wgt=get_post_meta($portfolio_shortcodes_cpt_id,'sp_des_font_wgt',true);
	$sp_des_fstyle=get_post_meta($portfolio_shortcodes_cpt_id,'sp_des_fstyle',true);
	$sp_des_text_trans=get_post_meta($portfolio_shortcodes_cpt_id,'sp_des_text_trans',true);
	$sp_overlay_clr=get_post_meta($portfolio_shortcodes_cpt_id,'sp_overlay_clr',true);
	list($r, $g, $b) = sscanf( $sp_overlay_clr, "#%02x%02x%02x");
	$sp_title_clr=get_post_meta($portfolio_shortcodes_cpt_id,'sp_title_clr',true);
	$sp_title_hov_clr=get_post_meta($portfolio_shortcodes_cpt_id,'sp_title_hov_clr',true);
	$sp_category_clr=get_post_meta($portfolio_shortcodes_cpt_id,'sp_category_clr',true);
	$sp_category_hov_clr=get_post_meta($portfolio_shortcodes_cpt_id,'sp_category_hov_clr',true);
	$sp_title_pop_clr=get_post_meta($portfolio_shortcodes_cpt_id,'sp_title_pop_clr',true);
	$sp_des_clr=get_post_meta($portfolio_shortcodes_cpt_id,'sp_des_clr',true);
	$sp_ftr_clr=get_post_meta($portfolio_shortcodes_cpt_id,'sp_ftr_clr',true);
	$sp_ftr_hvr_clr=get_post_meta($portfolio_shortcodes_cpt_id,'sp_ftr_hvr_clr',true);
	$sp_ftr_act_clr=get_post_meta($portfolio_shortcodes_cpt_id,'sp_ftr_act_clr',true);
	$sp_ftr_bdr_clr=get_post_meta($portfolio_shortcodes_cpt_id,'sp_ftr_bdr_clr',true);
	$sp_cat_arr=explode(',',$sp_cat_query);
	$sp_auth_query=get_post_meta($portfolio_shortcodes_cpt_id,'sp_auth_query',true);
	$sp_auth_arr=explode(',',$sp_auth_query);
	$sp_portfolio_orderby=get_post_meta($portfolio_shortcodes_cpt_id,'sp_portfolio_orderby',true);
	switch ($sp_column) {
		case 1: $sp_col=12; break;
		case 2: $sp_col=6; break;	
		case 3: $sp_col=4; break;
		case 4: $sp_col=3; break;
		default: $sp_col=4; break;
	}
	$post_type='spice_portfolio';
	$tax='sp_portfolio_categories';
	$term_args= array('hide_empty' => true, 'orderby' => 'id');
	$posts_per_page = $sp_counts;
	$tax_terms = get_terms($tax, $term_args);
	$j = 1;
	if($sp_filter=='no')
	{
		require('portfolio.php');
	}
	else
	{
		require('portfolio-filter.php');
	}
	?>

	
 	<script type="text/javascript">
	 jQuery(function(){
     jQuery( window ).on( "load", function() {
        var a = jQuery(".gall<?php echo intval($primary_key);?>").isotope({});
        var e = jQuery(".fil<?php echo intval($primary_key);?>").attr("data-filter");
        a.isotope({ filter: e });
      });
     jQuery(".fil<?php echo intval($primary_key);?>").on("click", "span", function () {
        var a = jQuery(".gall<?php echo intval($primary_key);?>").isotope({});
        var e = jQuery(this).attr("data-filter");
        a.isotope({ filter: e });
     });
     jQuery(".fil<?php echo intval($primary_key);?>").on("click", "span", function () {
        jQuery(this).addClass("active").siblings().removeClass("active");
     });
})
</script>
	<style type="text/css">
		.port<?php echo intval($primary_key);?> .portfolio-thumbnail .overlay 
		{
    		background: rgba( <?php echo esc_attr($r);?>,<?php echo esc_attr($g);?>,<?php echo esc_attr($b);?>,.9);
		}
		.port<?php echo intval($primary_key);?> .portfolio-thumbnail .title a,
		.port<?php echo intval($primary_key);?> .portfolio-thumbnail .title { 
			color: <?php echo esc_attr($sp_title_clr);?>;
		}
		.port<?php echo intval($primary_key);?> .portfolio-thumbnail .title a:hover{color: <?php echo esc_attr($sp_title_hov_clr);?>;}
		.port<?php echo intval($primary_key);?> .portfolio-thumbnail .post_cats a{color: <?php echo esc_attr($sp_category_clr);?>;}
		.port<?php echo intval($primary_key);?> .portfolio-thumbnail .post_cats {color: <?php echo esc_attr($sp_category_clr);?>;}
		.port<?php echo intval($primary_key);?> .portfolio-thumbnail .post_cats a:hover{color: <?php echo esc_attr($sp_category_hov_clr);?>;}
		.port<?php echo intval($primary_key);?> .port-modal-content .entry-header .entry-title {color: <?php echo esc_attr($sp_title_pop_clr);?>;}
		.port<?php echo intval($primary_key);?> .port-modal-content .entry-content,.port<?php echo intval($primary_key);?> .port-modal-content p{color: <?php echo esc_attr($sp_des_clr);?>;}
		.port<?php echo intval($primary_key);?> .filtering span {border-bottom: 1px solid <?php echo esc_attr($sp_ftr_bdr_clr);?>;}
		.port<?php echo intval($primary_key);?> .filtering span {color:<?php echo esc_attr($sp_ftr_clr);?>;}
		.port<?php echo intval($primary_key);?> .filtering span:hover {border-bottom: 1px solid <?php echo esc_attr($sp_ftr_hvr_clr);?>;}
		.port<?php echo intval($primary_key);?> .filtering span:hover {color:<?php echo esc_attr($sp_ftr_hvr_clr);?>;}
		.port<?php echo intval($primary_key);?> .filtering .active {border-color: <?php echo esc_attr($sp_ftr_act_clr);?>; color: <?php echo esc_attr($sp_ftr_act_clr);?>;}
		.port<?php echo intval($primary_key);?> .filtering span 
		{
			font-family: '<?php echo esc_attr($sp_filter_font_family);?>';
			font-size: <?php echo esc_attr($sp_filter_font_size);?>px;
			line-height: <?php echo esc_attr($sp_filter_lheight);?>px;
			font-weight: <?php echo esc_attr($sp_filter_font_wgt);?>;
			font-style: <?php echo esc_attr($sp_filter_fstyle);?>;
			text-transform: <?php echo esc_attr($sp_filter_text_trans);?>;
		}
		body .port<?php echo intval($primary_key);?> .portfolio-thumbnail .title a,
		body .port<?php echo intval($primary_key);?> .portfolio-thumbnail .title,
		body .port<?php echo intval($primary_key);?> .port-modal-content .entry-header .entry-title
		{
			font-family: '<?php echo esc_attr($sp_title_font_family);?>';
			font-size: <?php echo esc_attr($sp_title_font_size);?>px;
			line-height: <?php echo esc_attr($sp_title_lheight);?>px;
			font-weight: <?php echo esc_attr($sp_title_font_wgt);?>;
			font-style: <?php echo esc_attr($sp_title_fstyle);?>;
			text-transform: <?php echo esc_attr($sp_title_text_trans);?>;
		}
		body .port<?php echo intval($primary_key);?> .portfolio-thumbnail .post_cats a,
		body .port<?php echo intval($primary_key);?> .portfolio-thumbnail .post_cats
		{
			font-family: '<?php echo esc_attr($sp_cat_font_family);?>';
			font-size: <?php echo esc_attr($sp_cat_font_size);?>px;
			line-height: <?php echo esc_attr($sp_cat_lheight);?>px;
			font-weight: <?php echo esc_attr($sp_cat_font_wgt);?>;
			font-style: <?php echo esc_attr($sp_cat_fstyle);?>;
			text-transform: <?php echo esc_attr($sp_cat_text_trans);?>;
		}
		.port<?php echo intval($primary_key);?> .port-modal-content p, .port<?php echo intval($primary_key);?> .port-modal-content, .port<?php echo intval($primary_key);?> .port-modal-content .entry-content
		{
			font-family: '<?php echo esc_attr($sp_des_font_family);?>';
			font-size: <?php echo esc_attr($sp_des_font_size);?>px;
			line-height: <?php echo esc_attr($sp_des_lheight);?>px;
			font-weight: <?php echo esc_attr($sp_des_font_wgt);?>;
			font-style: <?php echo esc_attr($sp_des_fstyle);?>;
			text-transform: <?php echo esc_attr($sp_des_text_trans);?>;			
		}
	</style>
	<style type='text/css' id="sp-typo-<?php echo intval($primary_key);?>">
        <?php
        $filtering_font             = $sp_filter_font_family;
        $sp_port_title_font         = $sp_title_font_family;
        $sp_port_cat_font           = $sp_cat_font_family;
        $sp_port_des_font           = $sp_des_font_family;
        echo "@import url('https://fonts.googleapis.com/css2?family=".esc_attr($filtering_font).":wght@100;200;300;400;500;600;700;800;900&family=".esc_attr($sp_port_title_font).":wght@100;200;300;400;500;600;700;800;900&family=".esc_attr($sp_port_cat_font).":wght@100;200;300;400;500;600;700;800;900&family=".esc_attr($sp_port_des_font).":wght@100;200;300;400;500;600;700;800;900&display=swap');"; ?>
    </style>
	<?php
	return ob_get_clean();

}